package com.company;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите количество элементов массива: ");
        int numbers = in.nextInt();
        int array[] = new int[numbers];
        System.out.println("Вводите значения элементов масссива: ");

        for (int i = 0; i < numbers; i++) {
            array[i] = in.nextInt();
        }
        System.out.print("Введеный массив: ");

        for (int i = 0; i < numbers; i++) {
        }
        System.out.println(Arrays.toString(array));
        System.out.println();


        float second_array[] = new float[numbers];
        System.out.print("Массив увеличенный на 10%: ");
        for (int i = 0; i < numbers; i++) {
            second_array[i] = array[i] * 1.1f;
        }
        System.out.println(Arrays.toString(second_array));

        boolean bubble = false;
        while (!bubble) {
            bubble = true;
            for (int i = 0; i < numbers - 1; ++i) {
                if (second_array[i] < second_array[i + 1]) {
                    bubble = false;
                    float k = second_array[i];
                    second_array[i] = second_array[i + 1];
                    second_array[i + 1] = k;
                }
            }
        }
        System.out.println();
        System.out.print("Массив построеный по методу пузырька (от меньшего к большему): ");
        System.out.println(Arrays.toString(second_array));
    }
}
